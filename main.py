import json
from datetime import datetime
from plyer import notification


MONTHS = {
    1: "Jan",
    2: "Feb",
    3: "Mar",
    4: "Apr",
    5: "May",
    6: "June",
    7: "July",
    8: "Aug",
    9: "Sep",
    10: "Oct",
    11: "Nov",
    12: "Dec"
}


def notifier(notification_text, title, icon_name, app_name="Birthday Notifier"):
    """The function that handles how notification is to be made"""
    notification.notify(title=title, message=notification_text, app_name=app_name, app_icon=icon_name)


def day_maker(year, month, day):
    """Takes in month and day and converts into day. Year should have been passed
    in to check for leap year as a first argument but leap year checking is currently inactive"""
    month -= 1
    # if month < 2:
    return (month * 30) + day


def wish(name, age):
    """The function that wishes with name and age"""
    if age > 0:
        notification_text = f"Name: {name},\nAge: {age}"
    else:
        notification_text = f"Name: {name}"
    # notification.notify(title='Birthday Notification', message=notification_text, app_name="Birthday Notifier", app_icon='cake.ico')
    notifier(notification_text=notification_text, title="Birthday Notification", icon_name='cake.ico')


DATA = 'Data/data.json'
# now = datetime(2025,11,21) #For testing
now = datetime.now()
date_today = [now.year, now.month, now.day]
now_day = day_maker(now.year, now.month, now.day)


"""Leap year adjustment is not needed but can be reactivated by uncommenting the code below"""
# def leap_year(input_year):
#     if ((input_year % 400 == 0) or ((input_year % 4 == 0) and (input_year % 100 != 0))):
#         return True
#     else:
#         return False


def next_birthday():
    """Shows whose Birthday is up Next"""
    birthday_people_unsorted = {}
    birthday_people = {}
    birthday_days = []
    birthday_gone = []
    birthday_pending = []
    next_birthday = {}

    for key in data:  # Creating a new Dictionary just with name and days
        birthday_people_unsorted[day_maker(data[key][0], data[key][1], data[key][2])] = key
        birthday_days.append(day_maker(data[key][0], data[key][1], data[key][2]))
    birthday_days.sort()
    for birthday_day in birthday_days:
        birthday_people[birthday_day] = birthday_people_unsorted[birthday_day]
    # print(birthday_people)
    for day in list(birthday_people):
        if day <= now_day:
            birthday_gone.append(birthday_people[day])
        else:
            birthday_pending.append(birthday_people[day])

    if len(birthday_pending) == 0:
        next_birthday["Name"] = birthday_gone[0]
        next_birthday["Date"] = [data[birthday_gone[0]][1], data[birthday_gone[0]][2]]
        try:
            next_birthday["Age"] = now.year - data[birthday_gone[0]][0]
        except:
            next_birthday["Age"] = "Unavailable"
        else:
            next_birthday["Age"] = now.year - data[birthday_gone[0]][0]
    else:
        next_birthday["Name"] = birthday_pending[0]
        next_birthday["Date"] = [data[birthday_pending[0]][1], data[birthday_pending[0]][2]]
        try:
            next_birthday["Age"] = now.year - data[birthday_pending[0]][0]
        except:
            next_birthday["Age"] = "Unavailable"
        else:
            next_birthday["Age"] = now.year - data[birthday_pending[0]][0]
    notification_text = f"Name: {next_birthday['Name']}\nAge: {next_birthday['Age']}\nDate: {MONTHS[next_birthday['Date'][0]]} {next_birthday['Date'][1]}"
    # notification.notify(title='Next Birthday', message=notification_text, app_name="Birthday Notifier", app_icon='pending.ico')
    notifier(notification_text, 'Next Birthday', 'pending.ico')


try:
    d = open(DATA,'r')
except FileNotFoundError:
    raise Exception("You have not created the data file. Should be named 'data.json' by default in Data folder.")
else:
    data = json.load(d)  # Read the birthday file as json


for key in data:
    if data[key][1] == date_today[1]: #Checking for month
        # print(f"Num value increased to {num}")
        if data[key][2] == date_today[2]: #Checking for day
            # next_birthday()
            birthday_person = key
            try:
                age = date_today[0] - data[key][0] #Current year - year in DOB
            except:
                wish(birthday_person,0)
            else:
                wish(birthday_person,age)
next_birthday()
d.close()
