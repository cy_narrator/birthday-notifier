You need to store birthday information in a text file named 'data.json' that needs to be formatted in the way 'json_example.json' is formatted. That sort of look like this,

{

"Person Name 1":[DOB_Year,DOB_Month,DOB_day],
"Person Name 2":[DOB_Year,DOB_Month,DOB_day],
"Person Name 3":["X",DOB_Month,DOB_day]

}

Here, DOB_Year, DOB_Month and DOB_Day stand for year, month and day of birth respectively.

If you do not know the birth year, you can just put a "X" in place. It tells the program that you do not know the birth year.

IMPORTANT: Only use double quotes(") for Names in data.json. If you use single quotes('), you will get an error. You can use data_example.json as an example