import json
from datetime import datetime
today = datetime.now()


class Person:
    def __init__(self, name, date):
        self.year = date[0]
        try:
            self.year = int(self.year)
            # If someone refuses to provide their year of birth, a string may be placed such as "X" in place of year of birth
        except:
            self.year = 0
            # If there was a string in place of a number such as "X" then it means the correct year of birth is unavailable and be set to 0
        self.month = date[1]
        self.day = date[2]
        self.name = name
        self.value = (today.year * self.year) + (today.month * self.month) + (today.day * self.day)
        # I have a strong feeling this line is what is causing trouble, perhaps because my Math is weak

with open("data_example.json","r") as json_file:
    json_file = json.load(json_file)
    
persons = [Person(key, [value[i] for i in range(3)]) for key,value in json_file.items()]
# Create a persons list full of Person object reading the json_file
    
persons.sort(key=lambda x: x.value, reverse=False)
# Copied from stack overflow and I am not exactly sure how this works and what that lambda thing is (yet) but all I care is it will sort the persons list by taking value property of the Person object

new_dict = {person.name: [person.year, person.month, person.day] for person in persons}
# Finally create a new dictionary that is later to be dumped

print(new_dict)
# Print the result why not?

with open("data_example_sorted_desc_new.json","w") as new_json:
    json.dump(new_dict, new_json)
