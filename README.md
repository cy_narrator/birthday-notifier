# Birthday Notifier

This program is intended to run on Boot and is designed to give notification about birthday today or pending birthday of
your contacts as in data.json file.

To run this project,

1: Create a venv
```python -m venv venv```

2: Activate that venv
    On Windows,
    ```venv\Scripts\activate.bat```
    On GNU/Linux or MAC,
    ```venv/bin/activate.sh```

3: After that venv is activated and you can see a (venv) in front of your terminal, its time to install the requirements
into this venv:
```pip install -r requirements.txt```

4: Done. Now just run main.py
```python main.py```